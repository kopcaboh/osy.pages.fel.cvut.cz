---
title: "5. Vlákna a synchronizace"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Vlákna a synchronizace
Na tomto cvičení byste si měli vyzkoušet, jak vytvořit vlákno v jazyce
C, C++ či Rust s využitím knihovny [`pthread`][1] a jak vlákna
synchronizovat tak, aby nedošlo k poškození dat, se kterými se pracuje
z více vláken.

## Domácí příprava
Pro toto cvičení budete potřebovat znalosti o tom
- co jsou vlákna, mutexy a [`semafory`][2],
- jak tyto prostředky vytvoříte v jazyce C s využitím knihovny [`pthread`][1],
- jak se vlákna vytvářejí a ukončují,
- jaké problémy mohou nastávat při paralelním běhů vláken a
- jak psát programy tak, aby tyto problémy nenastaly.

Potřebná teorie byla vyložena na přednášce, včetně ukázek použití funkcí
knihovny [`pthread`][1]. Před cvičením je doporučeno podívat se na manuálové stránky
potřebných funkcí, zejména
- [`pthread_create`][3], [`pthread_join`][4],
- [`pthread_mutex_init`][5], [`pthread_mutex_destroy`][6],
  [`pthread_mutex_lock`][7], [`pthread_mutex_unlock`][7]
- [`sem_init`][8], [`sem_destroy`][9], [`sem_wait`][10], [`sem_post`][11].

Také se můžete podívat na videa [Unix Threads in C][12].

[1]: https://man7.org/linux/man-pages/man7/pthreads.7.html
[2]: https://man7.org/linux/man-pages/man7/sem_overview.7.html
[3]: https://man7.org/linux/man-pages/man3/pthread_create.3.html
[4]: https://man7.org/linux/man-pages/man3/pthread_join.3.html
[5]: https://man7.org/linux/man-pages/man3/pthread_mutex_init.3p.html
[6]: https://man7.org/linux/man-pages/man3/pthread_mutex_destroy.3p.html
[7]: https://man7.org/linux/man-pages/man3/pthread_mutex_lock.3p.html
[8]: https://man7.org/linux/man-pages/man3/sem_init.3.html
[9]: https://man7.org/linux/man-pages/man3/sem_destroy.3.html
[10]: https://man7.org/linux/man-pages/man3/sem_wait.3.html
[11]: https://man7.org/linux/man-pages/man3/sem_post.3.html
[12]: https://www.youtube.com/playlist?list=PLfqABt5AS4FmuQf70psXrsMLEDQXNkLq2

# Zadání úlohy
Implementujte v jazyce C, C++, nebo Rust vícevláknový program `prod-cons` splňující následující požadavky:

- V hlavním vlákně (funkce `main()`) se vytvoří jedno vlákno, kterému budeme
  říkat *producent* a dále `N` vláken *konzument*.

- Hodnota `N` bude zadávána jako parametr při spouštění programu (`argv[1]`).
  Pokud žádný parametr nebude zadán, bude `N` rovno `1`.

- Předpokládejte, že `N` bude v rozmezí 1 až počet CPU v systému
  (`sysconf(_SC_NPROCESSORS_ONLN)`). Pokud bude zadána jiná hodnota, program
  skončí s návratovým kódem `1`.

- Producent bude splňovat následující:

    - Bude číst ze standardního vstupu “příkazy” ve formě dvojic `<X> <slovo>`, kde
      `<X>` je celé nezáporné číslo a `<slovo>` je libovolná neprázdná sekvence znaků
      (kromě whitespace). `X` je od slova odděleno jednou mezerou, jednotlivé příkazy jsou
      od sebe vždy odděleny koncem řádku (`\n`). Na konci vstupu může, ale
      nemusí být konec řádku (`\n`).

      Validní vstup tedy může vypadat například takto:

      ```
      20 foo
      5 bar
      1 baz

      ```

      Délka slova je omezená pouze velikostí dostupné paměti. To
      znamená, že **slovo musíte mít v paměti uloženo jen jednou**. Na víc
      kopií nemusíte mít dost paměti.

      K načítání příkazů doporučujeme v C/C++ použít následující kód:
      ```C
      int ret, x;
      char *text;
      while ((ret = scanf("%d %ms", &x, &text)) == 2) {
          ...
      }
      ```
      Direktiva `%ms` (malloc string) způsobí, že `scanf` dynamicky alokuje
      takové množství paměti, které je potřeba pro uložení načítaného slova.
      Nezapomeňte potom tuto paměť uvolnit funkcí `free()`.

    - Pokud bude zadán neplatný příkaz (tj. neodpovídající předchozímu bodu),
      program skončí s návratovým kódem `1`.

    - Pro každý přečtený příkaz dynamicky alokuje (`malloc()`, `new`, …)
      datovou strukturu, uloží do ní `X` a `slovo` a zařadí ji na konec
      spojového seznamu.

- Každý konzument bude splňovat následující:

    - Bude ze začátku spojového seznamu vybírat položky vkládané producentem.

    - Pokud v seznamu žádná položka není, bude čekat, až tam producent něco
      přidá (bez spotřeby výpočetního času, žádný polling).

    - Pokud producent přidá `P` položek, vzbudí se maximálně `P` konzumentů,
      ostatní budou dále čekat.

    - Pro každou vyzvednutou položku konzument vypíše na standardní výstup
      řetězec “`Thread n: slovo slovo slovo...`”, kde `n` je číslo konzumenta
      (pořadí vytvoření konzumenta v rozsahu `1-N`) a slovo se opakuje X-krát
      (informace od producenta). Tento řetězec bude celý na jedné řádce
      ukončené `\n`.

- Pouze producent bude číst ze standardního vstupu.

- Pouze konzumenti budou zapisovat na standardní výstup.

- Standardní chybový výstup můžete použít k ladicím výpisům.

- Uzavření standardního vstupu je požadavkem na ukončení programu. Pokud není
  řečeno jinak, návratový kód bude `0`.

- Všechny platné “příkazy” zaslané na standardní vstup budou mít odpovídající
  řádku na standardním výstupu (nic se neztratí).

- Žádné čekání ve vašem programu by nemělo být implementováno formou pollingu
  (periodická kontrola, že se něco stalo).

- Program před ukončením dealokuje všechnu dynamicky alokovanou paměť.

Do odevzdávacího systému nahrajte:

{{< tabs "proglang" >}}
{{< tab "C/C++" >}}
Svůj zdrojový kód a `Makefile`, který vygeneruje program `prod-cons`
ve stejném adresáři jako `Makefile`.

Program překládejte s příznaky `-Wall -g -O2` a navíc s příznaky v
proměnné `EXTRA_CFLAGS` (evaluátor ji bude nastavovat podle potřeby).
Pokud tato proměnná není definována na příkazové řádce `make`,
nastavte její hodnotu na “`-fsanitize=address
-fno-omit-frame-pointer`” (viz např. [operátor
`?=`](https://www.gnu.org/software/make/manual/html_node/Flavors.html#index-conditional-variable-assignment)).
Pokud provádíte překlad a linkování odděleně, používejte příznaky v
`EXTRA_CFLAGS` také při linkování.

{{< /tab >}}
{{< tab "Rust" >}}
Cargo crate s názvem `prod-cons`. Závislosti můžete použít stejné,
jako v minulé úloze.

Standardní knihovna Rustu neobsahuje POSIX semafory, doporučujeme proto použít následující wrapper: [semaphore.rs](./semaphore.rs). Tento soubor vložte do stejného adresáře jako ostatní `.rs` soubory a importujte pomocí `mod semaphore;`. Pro použití nahlédněte do zdrojového kódu modulu.

{{< /tab >}}
{{< /tabs >}}

Překladač nesmí generovat žádná varování.

## Testovací vstup

Testovat váš program můžete např. následovně:

```sh
(echo "20 foo"; echo "3 bar"; echo "5 baz") | ./prod-cons 4
```

Program by měl vypsat zhruba toto (čísla threadů a pořadí řádků může být jiné):

	Thread 1: foo foo foo foo foo foo foo foo foo foo foo foo foo foo foo foo foo foo foo foo
	Thread 2: bar bar bar
	Thread 1: baz baz baz baz baz

Zkuste i chování při neplatném vstupu:

```sh
echo "invalid" | ./prod-cons 4
echo "Exit code: $?"
```
Výsledkem by mělo být:

    Exit code: 1

# Domácí příprava na další cvičení
Nastudujte si použití podmínkových proměnných a k tomu příslušné funkce v
knihovně [`pthread`][1]:
- [`pthread_cond_init`][13]
- [`pthread_cond_destroy`][13]
- [`pthread_cond_signal`][14]
- [`pthread_cond_broadcast`][15]
- [`pthread_cond_wait`][16]

[13]: https://man7.org/linux/man-pages/man3/pthread_cond_init.3p.html
[14]: https://man7.org/linux/man-pages/man3/pthread_cond_signal.3p.html
[15]: https://man7.org/linux/man-pages/man3/pthread_cond_broadcast.3p.html
[16]: https://man7.org/linux/man-pages/man3/pthread_cond_wait.3p.html
