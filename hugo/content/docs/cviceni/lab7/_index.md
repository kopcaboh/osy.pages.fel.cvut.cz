---
title: "7. Další metody synchronizace - dokončení"
weight: 1
# bookFlatSection: false
# bookToc: true
# bookHidden: false
# bookCollapseSection: false
# bookComments: true
---

# Další metody synchronizace - dokončení

Toto je konzultační cvičení. Docházka za toto cvičení se nepočítá.

# Domácí příprava na další cvičení

Seznamte se se základními instrukcemi architektury x86 a způsobem, jakým vkládat instrukce assembleru přímo do zdrojového kódu v jazyce C/C++.
